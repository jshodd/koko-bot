import requests
import os
# global commands are cached and only update every hour
# url = f'https://discord.com/api/v8/applications/{APP_ID}/commands'

# while guild commands update instantly
# they're much better for testing
APP_ID = os.getenv('APP_ID')
GUILD_ID = os.getenv('GUILD_ID')
BOT_TOKEN = os.getenv('BOT_TOKEN')

url = f'https://discord.com/api/v8/applications/{APP_ID}/guilds/{GUILD_ID}/commands'

json = {
  'name': 'start-server',
  'description': 'Start the foundry server if it isn\'t running',
  'options': []
}

response = requests.post(url, headers={
  'Authorization': f'Bot {BOT_TOKEN}'
}, json=json)

print(response.json())