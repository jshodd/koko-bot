import json
import boto3
import requests

from nacl.signing import VerifyKey
from nacl.exceptions import BadSignatureError

ssm = boto3.client('ssm')

PUBLIC_KEY = ssm.get_parameter(Name='/discord/bot/koko/public_key',WithDecryption=True)['Parameter']['Value']

def lambda_handler(event, context):
  try:
    print(event)
    body = json.loads(event['body'])
        
    signature = event['headers']['x-signature-ed25519']
    timestamp = event['headers']['x-signature-timestamp']

    # validate the interaction

    verify_key = VerifyKey(bytes.fromhex(PUBLIC_KEY))

    message = timestamp + json.dumps(body, separators=(',', ':'))
    
    try:
      verify_key.verify(message.encode(), signature=bytes.fromhex(signature))
    except BadSignatureError:
      return {
        'statusCode': 401,
        'body': json.dumps('invalid request signature')
      }
    
    # handle the interaction

    t = body['type']

    if t == 1:
      return {
        'statusCode': 200,
        'body': json.dumps({
          'type': 1
        })
      }
    elif t == 2:
      return command_handler(body)
    else:
      return {
        'statusCode': 400,
        'body': json.dumps('unhandled request type')
      }
  except:
    raise

def command_handler(body):
  command = body['data']['name']

  if command == 'start-server':
    FOUNDRY_MAGIC_LINK = ssm.get_parameter(Name='/discord/bot/koko/foundry_magic_link',WithDecryption=True)['Parameter']['Value']
    response = requests.get(FOUNDRY_MAGIC_LINK)
    if response.status_code == 200:

      return {
        'statusCode': 200,
        'body': json.dumps({
          'type': 4,
          'data': {
            'content': 'The server should be starting',
          }
        })
      }
    else:
      return {
        'statusCode': 500,
        'body': json.dumps('Something went wrong starting the server')
      }
  else:
    return {
      'statusCode': 400,
      'body': json.dumps('unhandled command')
    }